/**
 * Created by say on 23.05.2017.
 */
import axios from 'axios'
export default {
    server: 'http://api.brnv.rw/api/v1/',
    // SendValueToExportServices: function (selectedYear,selectedMonth,selectedVid,senddata,callback) {
    //     var url=this.server +'exportServicesValues/'+selectedYear+'/'+selectedMonth+'/'+selectedVid;
    //     axios.post(url,senddata)
    //         .then(response => {
    //             callback(null,response);
    //         })
    //         .catch(e => {
    //             callback(e);
    //         })
    // },
    SendValueToExportServices: function (senddata,callback) {
        var url=this.server +'exportServicesValues';
           axios.post(url, senddata )
            .then(response => {
                callback(null,response);
            })
            .catch(e => {
                callback(e);
            })
    },

    GetAllExportServices1: function (callback) {
        var url=this.server +'exportServices';
        axios.get(url)
            .then(response => {
                var resultArray = [];
                var tempresultArray = [];
                var serviceGroups = [];
                if (response.data) {
                    var res=response.data.data

                    var prevGroupNumber = 0;
                    var i=-1;
                    for (let key in res) {
                        if (prevGroupNumber != res[key].GroupNumber) {
                            i+=1;
                            resultArray[i]={GroupNumber:res[key].GroupNumber,GroupName:res[key].GroupLIst,GroupServices:[]}
                            prevGroupNumber = res[key].GroupNumber;
                        }
                        resultArray[i].GroupServices.push({
                            id: res[key].IdPred,
                            Title: res[key].LongName,
                            ShortTitle: res[key].ShortName
                        });
                    }
                }
                // console.log(resultArray);
                callback(null,resultArray);
            })
            .catch(e => {
                callback(e);
            })
    },
    GetOneExportServicesValue: function (selectedYear,selectedMonth,selectedVid,callback) {
        var url=this.server +'exportServicesValues/'+selectedYear+'/'+selectedMonth+'/'+selectedVid;
        var res ={'nValue':0,'nPlan':0,'nValueId':0,'currentAlertSryle':1,'alertMessage':'','showModal':false};
        axios.get(url)
            .then(response => {
                var res=response.data.data;
                if (res.length > 0) {
                    res.nValue = res[0].value;
                    res.nPlan = res[0].plan;
                    res.currentAlertSryle = (res.nValue==0 && res.nPlan==0) ? 1 : 0;
                    res.alertMessage="ID:"+res.nValueId+".";
                    res.showModal=true;
                }
                callback(null,res);
            })
            .catch(e => {
                callback(e);
            })
    },
    GetPeriodExportServicesValues: function (selectedYear,selectedMonth,callback) {
        var url=this.server +'exportServicesValues/'+selectedYear+'/'+selectedMonth;
        axios.get(url)
            .then(response => {
                var res=response.data.data;
                var resultArray = [];
                if (res.length > 0) {
                    var proc=0;
                    for (let key in res) {
                        proc=(res[key].plan==0) ? 0: Math.round(res[key].value/res[key].plan*100);
                        resultArray['id'+res[key].idpred]={ value: res[key].value, plan: res[key].plan ,procent: proc};
                    }
                    // self.serviceValues = resultArray;
                }
                callback(null,resultArray);
            })
            .catch(e => {
                callback(e);
            })
    }

}