import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import Scrollspy from 'vue2-scrollspy'
import Element from 'element-ui'
import elementLocale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/lib/theme-default/index.css'
import 'ptsans-npm-webfont/styleone.css'

Vue.use(Element, { locale: elementLocale });

Vue.use(Scrollspy);
export const eventBus = new Vue({
    methods: {
        SetServices(id) {
            this.$emit('serviceWasChanged', id);
        }
    }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
